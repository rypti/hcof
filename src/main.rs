use hex;
use openssl::hash::{hash, MessageDigest};
use std::fs::File;
use std::fs::OpenOptions;
use std::io::prelude::*;
use std::io::BufReader;
use std::mem::drop;
use std::sync::mpsc::channel;
use threadpool::ThreadPool;

fn main() {
    let args: Vec<String> = std::env::args().collect();

    if args.len() <= 1 {
        std::println!("Missing Arguments");
        std::process::exit(-1);
    }
    let name = &args[1];
    //let nb_thread = &args[2].parse::<usize>().unwrap();
    let file_input = File::open(name).expect("Can't open input file!");
    let reader = BufReader::new(file_input);
    let mut file_output = OpenOptions::new()
        .append(true)
        .create(true)
        .open(name.to_owned() + ".md5")
        .unwrap();
    let pool = ThreadPool::new(8);
    let (tx, rx) = channel();

    pool.execute(move || {
        for line in rx {
            writeln!(file_output, "{}", line).unwrap();
        }
    });

    for line in reader.lines() {
        let thread_tx = tx.clone();
        if let Ok(ip) = line {
            pool.execute(move || {
                thread_tx.send(make_hash(ip)).unwrap();
            });
        }
    }

    drop(tx);
    pool.join();
}

fn make_hash(line: String) -> String {
    return hex::encode(hash(MessageDigest::md5(), line.as_bytes()).unwrap());
}
